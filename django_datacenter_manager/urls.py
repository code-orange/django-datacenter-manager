from django.contrib import admin

from django.conf import settings
from django.urls import include, path
from django.urls import re_path

from django_datacenter_manager_api.django_datacenter_manager_api import urls as api_urls
from django_datacenter_manager_adblock.django_datacenter_manager_adblock import (
    urls as adblock_urls,
)
from django_datacenter_manager_whitelist.django_datacenter_manager_whitelist import (
    urls as whitelist_urls,
)

urlpatterns = [
    re_path(r"", include("user_sessions.urls", "user_sessions")),
    path("admin/", admin.site.urls),
    re_path(r"^watchman/", include("watchman.urls")),
    path("api/", include(api_urls)),
    path("adblock/", include(adblock_urls)),
    path("whitelist/", include(whitelist_urls)),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
